package main

import "gitlab.com/david-kalmakoff/blockchain/app/wallet/cli/cmd"

func main() {
	cmd.Execute()
}
