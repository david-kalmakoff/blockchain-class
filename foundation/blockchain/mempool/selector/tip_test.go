package selector

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/database"
)

func TestTipSelect(t *testing.T) {
	var in map[database.AccountID][]database.BlockTx
	err := json.Unmarshal([]byte(input), &in)
	if err != nil {
		t.Fatalf("Could not Unmarshal input: %v", err)
	}

	var out []database.BlockTx
	err = json.Unmarshal([]byte(output), &out)
	if err != nil {
		t.Fatalf("Could not Unmarshal output: %v", err)
	}

	ret := tipSelect(in, 4)

	diff := cmp.Diff(out, ret)
	if diff != "" {
		t.Fatalf("should return expected:\n%s", diff)
	}

	// fmt.Println("ret")
	// for i, f := range ret {
	// 	fmt.Printf("idx: %d, nonce: %d, tip: %d\n", i, f.Nonce, f.Tip)
	// }
}

var input = `{
  "Bill": [
      {
        "Nonce": 2,
        "Tip": 250
      },
      {
        "Nonce": 1,
        "Tip": 150
      }
    ],
    "Pavl": [
      {
        "Nonce": 2,
        "Tip": 200
      },
      {
        "Nonce": 1,
        "Tip": 75
      }
    ],
    "Edua": [
      {
        "Nonce": 2,
        "Tip": 75
      },
      {
        "Nonce": 1,
        "Tip": 100
      }
    ]
}`

var output = `[
    {
      "Nonce": 1,
      "Tip": 150
    },
    {
      "Nonce": 1,
      "Tip": 75
    },
    {
      "Nonce": 1,
      "Tip": 100
    },
    {
      "Nonce": 2,
      "Tip": 75
    }
]`
