package selector

import (
	"fmt"
	"strings"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/database"
)

// List of strategies
const (
	StrategyTip         = "tip"
	StrategyTipAdvanced = "tip_advanced"
)

// Map of different select strategies
var strategies = map[string]Func{
	StrategyTip:         tipSelect,
	StrategyTipAdvanced: advancedTipSelect,
}

// Func defines a function
type Func func(transactions map[database.AccountID][]database.BlockTx, howMany int) []database.BlockTx

// Retrieve returns the specific strategy function
func Retrieve(strategy string) (Func, error) {
	fn, exists := strategies[strings.ToLower(strategy)]
	if !exists {
		return nil, fmt.Errorf("strategy %q does not exist", strategy)
	}
	return fn, nil
}

// ============================================================================

// byNonce provides sorting support
type byNonce []database.BlockTx

// Len returns the number of txs in the list
func (bn byNonce) Len() int {
	return len(bn)
}

// Less sorts in ascending order
func (bn byNonce) Less(i, j int) bool {
	return bn[i].Nonce < bn[j].Nonce
}

// Swap moves transactions
func (bn byNonce) Swap(i, j int) {
	bn[i], bn[j] = bn[j], bn[i]
}

// ============================================================================

// byTip provides sorting support
type byTip []database.BlockTx

// Len returns the number of txs in the list
func (bt byTip) Len() int {
	return len(bt)
}

// Less sorts in ascending order
func (bt byTip) Less(i, j int) bool {
	return bt[i].Tip < bt[j].Tip
}

// Swap moves transactions
func (bt byTip) Swap(i, j int) {
	bt[i], bt[j] = bt[j], bt[i]
}
