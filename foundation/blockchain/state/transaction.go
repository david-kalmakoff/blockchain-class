package state

import "gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/database"

func (s *State) UpsertWalletTransaction(signedTx database.SignedTx) error {
	// Up to the wallet to make sure the account has proper balance

	// Check the signed transaction
	if err := signedTx.Validate(s.genesis.ChainID); err != nil {
		return err
	}

	const onUnitOfGas = 1
	tx := database.NewBlockTx(signedTx, s.genesis.GasPrice, onUnitOfGas)
	if err := s.mempool.Upsert(tx); err != nil {
		return err
	}

	return nil
}
