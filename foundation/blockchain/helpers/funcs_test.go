package helpers_test

import (
	"testing"

	"github.com/ethereum/go-ethereum/crypto"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/helpers"
)

func TestFuncs(t *testing.T) {
	key, err := helpers.GeneratePrivateKey()
	if err != nil {
		t.Fatalf("\tF\tshould generate private key: %v", err)
	}

	addr, err := helpers.GetAddressFromPrivateKey(key)
	if err != nil {
		t.Fatalf("\tF\tshould get address: %v", err)
	}

	b := helpers.GetECDSAFromPrivateKey(key)

	pKey, err := crypto.ToECDSA(b)
	if err != nil {
		t.Fatalf("\tF\tshould get private key: %v", err)
	}

	pAddr, err := helpers.GetAddressFromPrivateKey(pKey)
	if err != nil {
		t.Fatalf("\tF\tshould get address: %v", err)
	}

	if addr != pAddr {
		t.Fatalf("\tF\tshould have mathing addresses: %v", err)
	}
}
