package database

import (
	"bytes"
	"crypto/ecdsa"
	"encoding/hex"
	"errors"
	"fmt"
	"math/big"
	"time"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/signature"
)

// Transaction between two parties
type Tx struct {
	ChainID uint16    `json:"chain_id"`
	Nonce   uint64    `json:"nonce"`
	FromID  AccountID `json:"from"`
	ToID    AccountID `json:"to"`
	Value   uint64    `json:"value"`
	Tip     uint64    `json:"tip"`
	Data    []byte    `json:"data"`
}

// NewTx creates a new transaction
func NewTx(chainID uint16, nonce uint64, fromID AccountID, toID AccountID, value uint64, tip uint64, data []byte) (Tx, error) {
	if !fromID.IsAccountID() {
		return Tx{}, errors.New("from account is not properly formatted")
	}
	if !toID.IsAccountID() {
		return Tx{}, errors.New("to account is not properly formatted")
	}

	tx := Tx{
		ChainID: chainID,
		Nonce:   nonce,
		FromID:  fromID,
		ToID:    toID,
		Value:   value,
		Tip:     tip,
		Data:    data,
	}

	return tx, nil
}

// Sign uses the specific private key to sign the transaction
func (tx Tx) Sign(privateKey *ecdsa.PrivateKey) (SignedTx, error) {
	// Sign the transaction
	v, r, s, err := signature.Sign(tx, privateKey)
	if err != nil {
		return SignedTx{}, err
	}

	signedTx := SignedTx{
		Tx: tx,
		V:  v,
		R:  r,
		S:  s,
	}

	return signedTx, nil
}

// ============================================================================

// SignedTx is the signed transaction
type SignedTx struct {
	Tx
	V *big.Int `json:"v"` // Recovery identifier
	R *big.Int `json:"r"` // First coordinate
	S *big.Int `json:"s"` // Second coordinate
}

// Validate verifies transaction has proper signature
func (tx SignedTx) Validate(chainID uint16) error {
	if tx.ChainID != chainID {
		return fmt.Errorf("invalid chain id, got[%d] exp[%d]", tx.ChainID, chainID)
	}

	if !tx.FromID.IsAccountID() {
		return errors.New("from account is not properly formatted")
	}

	if !tx.ToID.IsAccountID() {
		return errors.New("to account is not properly formatted")
	}

	if tx.FromID == tx.ToID {
		return fmt.Errorf("transaction invalid, sending to yourself, from %s, to %s", tx.FromID, tx.ToID)
	}

	if err := signature.VerifySignature(tx.V, tx.R, tx.S); err != nil {
		return err
	}

	address, err := signature.FromAddress(tx.Tx, tx.V, tx.R, tx.S)
	if err != nil {
		return err
	}

	if address != string(tx.FromID) {
		return errors.New("signature address doesn't match from address")
	}

	return nil
}

// SignatureString returns the signature as a string
func (tx SignedTx) SignatureString() string {
	return signature.SignatureString(tx.V, tx.R, tx.S)
}

// String implements the Stringer interface for logging
func (tx SignedTx) String() string {
	return fmt.Sprintf("%s:%d", tx.FromID, tx.Nonce)
}

// ============================================================================

// BlockTx represents the transaction recorded in a block
type BlockTx struct {
	SignedTx
	Timestamp uint64 `json:"timestamp"` // Time transaction was recieved
	GasPrice  uint64 `json:"gas_price"` // Price of units paid for fees
	GasUnits  uint64 `json:"gas_units"` // Number of units used for transaction
}

// NewBlockTx creates a new block transaction
func NewBlockTx(signedTx SignedTx, gasPrice uint64, unitsOfGas uint64) BlockTx {
	return BlockTx{
		SignedTx:  signedTx,
		Timestamp: uint64(time.Now().UTC().UnixMilli()),
		GasPrice:  gasPrice,
		GasUnits:  unitsOfGas,
	}
}

// Hash implements the merkle Hashable interface
func (tx BlockTx) Hash() ([]byte, error) {
	str := signature.Hash(tx)

	// Need to remove the 0x from the hash
	return hex.DecodeString(str[2:])
}

// Equals implemets merkle hashable interface
func (tx BlockTx) Equals(otherTx BlockTx) bool {
	txSig := signature.ToSignatureBytes(tx.V, tx.R, tx.S)
	otherTxSig := signature.ToSignatureBytes(otherTx.R, otherTx.R, otherTx.S)

	return tx.Nonce == otherTx.Nonce && bytes.Equal(txSig, otherTxSig)
}
