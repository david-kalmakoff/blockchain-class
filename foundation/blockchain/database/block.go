package database

import (
	"context"
	"crypto/rand"
	"math"
	"math/big"
	"time"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/merkle"
	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/signature"
)

// BlockData represents what can be serialized to disk and over the network.
type BlockData struct {
	Hash   string      `json:"hash"`
	Header BlockHeader `json:"block"`
	Trans  []BlockTx   `json:"trans"`
}

// NewBlockData constructs block data from a block
func NewBlockData(block Block) BlockData {
	blockData := BlockData{
		Hash:   block.Hash(),
		Header: block.Header,
		Trans:  block.MerkleTree.Values(),
	}

	return blockData
}

// ToBlock converts a storage block into a database block
func ToBlock(blockData BlockData) (Block, error) {
	tree, err := merkle.NewTree(blockData.Trans)
	if err != nil {
		return Block{}, err
	}

	block := Block{
		Header:     blockData.Header,
		MerkleTree: tree,
	}

	return block, nil
}

// ============================================================================

// BlockHeader represents common information required for each block.
type BlockHeader struct {
	Number        uint64    `json:"number"`          // Eth: Block number in the chain
	PrevBlockHash string    `json:"prev_block_hash"` // BTC: Hash of the previous block in the chain
	TimeStamp     uint64    `json:"timestamp"`       // BTC: Time the block was mined
	BeneficiaryID AccountID `json:"beneficiary"`     // ETH: The account who receives the fees
	Difficulty    uint16    `json:"difficulty"`      // ETH: Number of 0's needed to solve hash
	MiningReward  uint64    `json:"mining_reward"`   // ETH: The reward for mining the block
	StateRoot     string    `json:"state_root"`      // ETH: Hash of the accounts and their balances
	TransRoot     string    `json:"trans_root"`      // Both: Merkle tree root hash for the transactions
	Nonce         uint64    `json:"nonce"`           // Both: Value identified to solve the hash
}

// Block represents a group of transactions batched together
type Block struct {
	Header     BlockHeader
	MerkleTree *merkle.Tree[BlockTx]
}

// POWArgs represents the set of arguments required to run POW.
type POWArgs struct {
	BeneficiaryID AccountID
	Difficulty    uint16
	MiningReward  uint64
	PrevBlock     Block
	StateRoot     string
	Trans         []BlockTx
	EvHandler     func(v string, args ...any)
}

// POW constructs a new block and performs the work to find a nonce that solves
// the cryptographic POW puzzle.
func POW(ctx context.Context, args POWArgs) (Block, error) {
	// When mining the first block, the previous block's hash will be zero.
	prevBlockHash := signature.ZeroHash
	if args.PrevBlock.Header.Number > 0 {
		prevBlockHash = args.PrevBlock.Hash()
	}

	// Construct a merkle tree from the transaction for this block. The root
	// of this tree will be part of the block to be mined
	tree, err := merkle.NewTree(args.Trans)
	if err != nil {
		return Block{}, err
	}

	// Construct the block to be mined
	block := Block{
		Header: BlockHeader{
			Number:        args.PrevBlock.Header.Number + 1,
			PrevBlockHash: prevBlockHash,
			TimeStamp:     uint64(time.Now().UTC().UnixMilli()),
			BeneficiaryID: args.BeneficiaryID,
			Difficulty:    args.Difficulty,
			MiningReward:  args.MiningReward,
			StateRoot:     args.StateRoot,
			TransRoot:     tree.RootHex(),
			Nonce:         0, // Will be identified by the POW algo
		},
		MerkleTree: tree,
	}

	// Perform the proof of work mining operation
	if err := block.performPOW(ctx, args.EvHandler); err != nil {
		return Block{}, err
	}

	return block, nil
}

// performPOW does the work of mining to find a valid hash for a specified
// block. Pointer semantics are being used since a nonce is being discovered
func (b *Block) performPOW(ctx context.Context, ev func(v string, args ...any)) error {
	ev("database: PerformPOW: MINING: started")
	defer ev("database: PerformPOW: MINING: completed")

	for _, tx := range b.MerkleTree.Values() {
		ev("database: PerformPOW: MINING: tx[%s]", tx)
	}

	// Choose random starting point for the nonce. After this, the nonce
	// will be incremented by 1 until a solution is found by us or another node.
	nBig, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt64))
	if err != nil {
		return ctx.Err()
	}
	b.Header.Number = nBig.Uint64()

	ev("viewer: PerformPOW: MINING: running")

	// Loop until we or another node finds a solution for that next block.
	var attempts uint64
	for {
		attempts++
		if attempts%1_000_000 == 0 {
			ev("viewer: PerformPOW: MINING: running: attempts[%d]", attempts)
		}

		// Did we timeout trying to solve the problem.
		if ctx.Err() != nil {
			ev("viewer: PerformPOW: MINING: CANCELLED")
			return ctx.Err()
		}

		// Hash the block and check if we have solved the puzzle.
		hash := b.Hash()
		if !isHashSolved(b.Header.Difficulty, hash) {
			b.Header.Nonce++
			continue

		}

		ev("viewer: PerformPOW: MINING: SOLVED: prevBlk[%s]: newBlk[%s]", b.Header.PrevBlockHash, hash)
		ev("viewer: PerformPOW: MINING: attempts[%d]", attempts)

		return nil
	}
}

// Hash returns the uniquw hash for the Block
func (b Block) Hash() string {
	if b.Header.Number == 0 {
		return signature.ZeroHash
	}

	return signature.Hash(b.Header)
}

// isHashSolved checks the hash to make sure it complies with
// the POW rules. We need to match the difficulty number of 0's.
func isHashSolved(difficulty uint16, hash string) bool {
	const match = "0x000000000000000000"

	if len(hash) != 66 {
		return false
	}

	difficulty += 2
	return hash[:difficulty] == match[:difficulty]
}
