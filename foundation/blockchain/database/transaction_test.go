package database_test

import (
	"testing"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/database"
	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/helpers"
)

func TestNewTx(t *testing.T) {
	tests := []struct {
		name string
		from database.AccountID
		to   database.AccountID
		pass bool
	}{
		{
			name: "valid to and from addresses",
			from: "0xc23199c3865683C1DE4356D8263DDa0CDA2A13ee",
			to:   "0x40BD5f9066ACAC85297B6C8F28FE063eE890Eb59",
			pass: true,
		},
		{
			name: "invalid from address",
			from: "0xc23199c3865683C1DE4356D8263DDa0CDA2A13e",
			to:   "0x40BD5f9066ACAC85297B6C8F28FE063eE890Eb59",
			pass: false,
		},
		{
			name: "invalid to address",
			from: "0xc23199c3865683C1DE4356D8263DDa0CDA2A13ee",
			to:   "0x40BD5f9066ACAC85297B6C8F28FE063eE890Eb5",
			pass: false,
		},
	}

	for i, tt := range tests {
		t.Logf("\nRunning #%d, %s", i, tt.name)
		test := func(t *testing.T) {
			tx, err := database.NewTx(1, 1, tt.from, tt.to, 1000, 0, nil)

			switch tt.pass {
			case true:
				if err != nil {
					t.Fatalf("\tF\tshould create tx: %v", err)
				}
				t.Logf("\tT\tshould create tx")

			case false:
				if err == nil {
					t.Fatalf("\tF\tshould not create tx: %#v", tx)
				}
				t.Logf("\tT\tshould not create tx")
			}
		}

		t.Run(tt.name, test)
	}
}

func TestSign(t *testing.T) {
	key, err := helpers.GeneratePrivateKey()
	if err != nil {
		t.Fatalf("\tF\tshould generate key: %v", err)
	}
	addr, err := helpers.GetAddressFromPrivateKey(key)
	if err != nil {
		t.Fatalf("\tF\tshould get address: %v", err)
	}

	tx, err := database.NewTx(
		1, 1, addr, "0xc23199c3865683C1DE4356D8263DDa0CDA2A13ee", 1000, 0, nil,
	)
	if err != nil {
		t.Fatalf("\tF\tshould create tx: %v", err)
	}

	sTx, err := tx.Sign(key)
	if err != nil {
		t.Fatalf("\tF\tshould sign tx: %v", err)
	}

	if err := sTx.Validate(1); err != nil {
		t.Fatalf("\tF\tshould validate signed tx: %v", err)
	}
}
