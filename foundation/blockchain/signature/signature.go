package signature

import (
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

// ZeroHash is a has of zeros
const ZeroHash string = "0x0000000000000000000000000000000000000000000000000000000000000000"

// extendedID is an arbitrary number for signing messages
const extendedID = 85

// ============================================================================

// Hash returns a unique string
func Hash(value any) string {
	data, err := json.Marshal(value)
	if err != nil {
		return ZeroHash
	}

	hash := sha256.Sum256(data)
	return hexutil.Encode(hash[:])
}

// Sign uses the private key to sign the data
func Sign(value any, privateKey *ecdsa.PrivateKey) (v, r, s *big.Int, err error) {
	// Prepare the data for signing
	data, err := stamp(value)
	if err != nil {
		return nil, nil, nil, err
	}

	// Sign the hash with the private key to produce a signature
	sig, err := crypto.Sign(data, privateKey)
	if err != nil {
		return nil, nil, nil, err
	}

	// Extract the bytes for public key
	publicKeyOrg := privateKey.Public()
	publicKeyECDSA, ok := publicKeyOrg.(*ecdsa.PublicKey)
	if !ok {
		return nil, nil, nil, errors.New("error casting public key to ECDSA")
	}
	publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)

	// Check the public key validate the data and signature
	rs := sig[:crypto.RecoveryIDOffset]
	if !crypto.VerifySignature(publicKeyBytes, data, rs) {
		return nil, nil, nil, errors.New("invalid signature produced")
	}

	// Convert to [R|S|V]
	v, r, s = toSignatureValues(sig)

	return v, r, s, nil
}

// VerifySignature verifies that the signature matches our standards
func VerifySignature(v, r, s *big.Int) error {
	// Check recovery is either 0 or 1
	uintV := v.Uint64() - extendedID
	if uintV != 0 && uintV != 1 {
		return errors.New("invalid recovery id")
	}

	// Check the values are valid
	if !crypto.ValidateSignatureValues(byte(uintV), r, s, false) {
		return errors.New("invalid signature values")
	}

	return nil
}

// FromAddress extracts the address for the account that signed the data
func FromAddress(value any, v, r, s *big.Int) (string, error) {
	// Prepare the data for public key extraction
	data, err := stamp(value)
	if err != nil {
		return "", err
	}

	// Convert to bytes
	sig := ToSignatureBytes(v, r, s)

	// Capture the public key associated with this data and signature
	publicKey, err := crypto.SigToPub(data, sig)
	if err != nil {
		return "", err
	}

	// Extract the account address from the public key
	return crypto.PubkeyToAddress(*publicKey).String(), nil
}

// ToSignatureBytes converts r,s,v to bytes, removing extendedID
func ToSignatureBytes(v, r, s *big.Int) []byte {
	sig := make([]byte, crypto.SignatureLength)

	rBytes := make([]byte, 32)
	r.FillBytes(rBytes)
	copy(sig, rBytes)

	sBytes := make([]byte, 32)
	s.FillBytes(sBytes)
	copy(sig[32:], sBytes)

	sig[64] = byte(v.Uint64() - extendedID)

	return sig
}

// SignatureString returns the signature as a string
func SignatureString(v, r, s *big.Int) string {
	return hexutil.Encode(ToSignatureBytesWithExtendedID(v, r, s))
}

func ToSignatureBytesWithExtendedID(v, r, s *big.Int) []byte {
	sig := ToSignatureBytes(v, r, s)
	sig[64] = byte(v.Uint64())

	return sig
}

// ============================================================================

// stamp returns 32 byte hash for the data with our stamp
func stamp(value any) ([]byte, error) {
	v, err := json.Marshal(value)
	if err != nil {
		return nil, err
	}

	// Stamp creates unique signatures for the blockchain
	stamp := []byte(fmt.Sprintf("\x19Extended Step Signed Message:\n%d", len(v)))

	// Hash stamp and tx in final 32 byte array
	data := crypto.Keccak256(stamp, v)

	return data, nil
}

// toSignatureValues converts signature to r, s, v values
func toSignatureValues(sig []byte) (v, r, s *big.Int) {
	r = big.NewInt(0).SetBytes(sig[:32])
	s = big.NewInt(0).SetBytes(sig[32:64])
	v = big.NewInt(0).SetBytes([]byte{sig[64] + extendedID})

	return v, r, s
}
